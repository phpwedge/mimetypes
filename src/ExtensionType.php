<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace PhpWedge\Core;

/**
 * Class ExtensionType
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
 *
 * @package PhpWedge\Core
 */
interface ExtensionType
{
    /**
     * Mime-type: AAC audio file
     */
    const TYPE_AAC = '.aac';

    /**
     * Mime-type: AbiWord document
     */
    const TYPE_ABW = '.abw';

    /**
     * Mime-type: Archive document (multiple files embedded)
     */
    const TYPE_ARC = '.arc';

    /**
     * Mime-type: AVI: Audio Video Interleave
     */
    const TYPE_AVI = '.avi';

    /**
     * Mime-type: Amazon Kindle eBook format
     */
    const TYPE_AZW = '.azw';

    /**
     * Mime-type: Any kind of binary data
     */
    const TYPE_BIN = '.bin';

    /**
     * Mime-type: BZip archive
     */
    const TYPE_BZ = '.bz';

    /**
     * Mime-type: BZip2 archive
     */
    const TYPE_BZ2 = '.bz2';

    /**
     * Mime-type: C-Shell script
     */
    const TYPE_CSH = '.csh';

    /**
     * Mime-type: Cascading Style Sheets (CSS)
     */
    const TYPE_CSS = '.css';

    /**
     * Mime-type: Comma-separated values (CSV)
     */
    const TYPE_CSV = '.csv';

    /**
     * Mime-type: Microsoft Word
     */
    const TYPE_DOC = '.doc';

    /**
     * Mime-type: Electronic publication (EPUB)
     */
    const TYPE_EPUB = '.epub';

    /**
     * Mime-type: Graphics Interchange Format (GIF)
     */
    const TYPE_GIF = '.gif';

    /**
     * Mime-type: HyperText Markup Language (HTML)
     */
    const TYPE_HTM = '.htm';

    /**
     * Mime-type: HyperText Markup Language (HTML)
     */
    const TYPE_HTML = '.html';

    /**
     * Mime-type: Icon format
     */
    const TYPE_ICO = '.ico';

    /**
     * Mime-type: iCalendar format
     */
    const TYPE_ICS = '.ics';

    /**
     * Mime-type: Java Archive (JAR)
     */
    const TYPE_JAR = '.jar';

    /**
     * Mime-type: JPEG images
     */
    const TYPE_JPEG = '.jpeg';

    /**
     * Mime-type: JPEG images
     */
    const TYPE_JPG = '.jpg';

    /**
     * Mime-type: JavaScript (ECMAScript)
     */
    const TYPE_JS = '.js';

    /**
     * Mime-type: JSON format
     */
    const TYPE_JSON = '.json';

    /**
     * Mime-type: Markdown document
     */
    const TYPE_MD = '.md';

    /**
     * Mime-type: Musical Instrument Digital Interface (MIDI)
     */
    const TYPE_MID = '.mid';

    /**
     * Mime-type: Musical Instrument Digital Interface (MIDI)
     */
    const TYPE_MIDI = '.midi';

    /**
     * Mime-type: MPEG Video
     */
    const TYPE_MPEG = '.mpeg';

    /**
     * Mime-type: Apple Installer Package
     */
    const TYPE_MPKG = '.mpkg';

    /**
     * Mime-type: OpenDocuemnt presentation document
     */
    const TYPE_ODP = '.odp';

    /**
     * Mime-type: OpenDocuemnt spreadsheet document
     */
    const TYPE_ODS = '.ods';

    /**
     * Mime-type: OpenDocument text document
     */
    const TYPE_ODT = '.odt';

    /**
     * Mime-type: OGG audio
     */
    const TYPE_OGA = '.oga';

    /**
     * Mime-type: OGG video
     */
    const TYPE_OGV = '.ogv';

    /**
     * Mime-type: OGG
     */
    const TYPE_OGX = '.ogx';

    /**
     * Mime-type: Portable Network Graphics
     */
    const TYPE_PNG = '.png';

    /**
     * Mime-type: Adobe Portable Document Format (PDF)
     */
    const TYPE_PDF = '.pdf';

    /**
     * Mime-type: Microsoft PowerPoint
     */
    const TYPE_PPT = '.ppt';

    /**
     * Mime-type: RAR archive
     */
    const TYPE_RAR = '.rar';

    /**
     * Mime-type: Rich Text Format (RTF)
     */
    const TYPE_RTF = '.rtf';

    /**
     * Mime-type: Bourne shell script
     */
    const TYPE_SH = '.sh';

    /**
     * Mime-type: Scalable Vector Graphics (SVG)
     */
    const TYPE_SVG = '.svg';

    /**
     * Mime-type: Small web format (SWF) or Adobe Flash document
     */
    const TYPE_SWF = '.swf';

    /**
     * Mime-type: Tape Archive (TAR)
     */
    const TYPE_TAR = '.tar';

    /**
     * Mime-type: Tagged Image File Format (TIFF)
     */
    const TYPE_TIF = '.tif';

    /**
     * Mime-type: Tagged Image File Format (TIFF)
     */
    const TYPE_TIFF = '.tiff';

    /**
     * Mime-type: TrueType Font
     */
    const TYPE_TTF = '.ttf';

    /**
     * Mime-type: Plain Text File
     */
    const TYPE_TXT = '.txt';

    /**
     * Mime-type: Microsoft Visio
     */
    const TYPE_VSD = '.vsd';

    /**
     * Mime-type: Waveform Audio Format
     */
    const TYPE_WAV = '.wav';

    /**
     * Mime-type: WEBM audio
     */
    const TYPE_WEBA = '.weba';

    /**
     * Mime-type: WEBM video
     */
    const TYPE_WEBM = '.webm';

    /**
     * Mime-type: WEBP image
     */
    const TYPE_WEBP = '.webp';

    /**
     * Mime-type: Web Open Font Format (WOFF)
     */
    const TYPE_WOFF = '.woff';

    /**
     * Mime-type: Web Open Font Format (WOFF)
     */
    const TYPE_WOFF2 = '.woff2';

    /**
     * Mime-type: XHTML
     */
    const TYPE_XHTML = '.xhtml';

    /**
     * Mime-type: Microsoft Excel
     */
    const TYPE_XLS = '.xls';

    /**
     * Mime-type: XML
     */
    const TYPE_XML = '.xml';

    /**
     * Mime-type: XUL
     */
    const TYPE_XUL = '.xul';

    /**
     * Mime-type: ZIP archive
     */
    const TYPE_ZIP = '.zip';

    /**
     * Mime-type: 3GPP audio/video container
     */
    const TYPE_3GP = '.3gp';

    /**
     * Mime-type: 3GPP2 audio/video container
     */
    const TYPE_3G2 = '.3g2';

    /**
     * Mime-type: 7-zip archive
     */
    const TYPE_7Z = '.7z';
}
