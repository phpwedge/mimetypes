[![CircleCI](https://circleci.com/bb/phpwedge/mimetypes.svg?style=svg)](https://circleci.com/bb/phpwedge/mimetypes)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9d1c92070938451caa48519735d87202)](https://www.codacy.com/app/PhpWedge/mimetypes?utm_source=phpwedge@bitbucket.org&amp;utm_medium=referral&amp;utm_content=phpwedge/mimetypes&amp;utm_campaign=Badge_Grade)
[![Latest Stable Version](https://poser.pugx.org/phpwedge/mimetypes/v/stable)](https://packagist.org/packages/phpwedge/mimetypes)
[![Total Downloads](https://poser.pugx.org/phpwedge/mimetypes/downloads)](https://packagist.org/packages/phpwedge/mimetypes)
[![License](https://poser.pugx.org/phpwedge/mimetypes/license)](https://packagist.org/packages/phpwedge/mimetypes)
# PhpWedge MimeType Package
>This package contains the most commonly used (on web) Mime-Types and file extensions.
### How to setup?
#### via composer cli
````bash
composer require phpwedge/mimetypes
````
#### via composer.json
````json
  "require": {
    "phpwedge/mimetypes": "^1.0.0"
  }
````
### How to use?
#### for extensions
````php
<?php
use PhpWedge\Core\ExtensionType;

$fileName = 'myfile' . ExtensionType::TYPE_TXT;
````
#### for mime types
````php
<?php
use PhpWedge\Core\MimeType;

header('Content-type: ' . MimeType::TYPE_HTML);
echo '<h1>Hello World!</h1>';

````
### Contribution
> If you miss any kind of Mime type or extension, please create an issue or a pull request!
### References
* [https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types)
