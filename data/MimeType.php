/** Mime-type: AAC audio file */
const TYPE_AAC = 'audio/aac';

/** Mime-type: AbiWord document */
const TYPE_ABW = 'application/x-abiword';

/** Mime-type: Archive document (multiple files embedded) */
const TYPE_ARC = 'application/octet-stream';

/** Mime-type: AVI: Audio Video Interleave */
const TYPE_AVI = 'video/x-msvideo';

/** Mime-type: Amazon Kindle eBook format */
const TYPE_AZW = 'application/vnd.amazon.ebook';

/** Mime-type: Any kind of binary data */
const TYPE_BIN = 'application/octet-stream';

/** Mime-type: BZip archive */
const TYPE_BZ = 'application/x-bzip';

/** Mime-type: BZip2 archive */
const TYPE_BZ2 = 'application/x-bzip2';

/** Mime-type: C-Shell script */
const TYPE_CSH = 'application/x-csh';

/** Mime-type: Cascading Style Sheets (CSS) */
const TYPE_CSS = 'text/css';

/** Mime-type: Comma-separated values (CSV) */
const TYPE_CSV = 'text/csv';

/** Mime-type: Microsoft Word */
const TYPE_DOC = 'application/msword';

/** Mime-type: Electronic publication (EPUB) */
const TYPE_EPUB = 'application/epub+zip';

/** Mime-type: Graphics Interchange Format (GIF) */
const TYPE_GIF = 'image/gif';

/** Mime-type: HyperText Markup Language (HTML) */
const TYPE_HTM = 'text/html';

/** Mime-type: HyperText Markup Language (HTML) */
const TYPE_HTML = 'text/html';

/** Mime-type: Icon format */
const TYPE_ICO = 'image/x-icon';

/** Mime-type: iCalendar format */
const TYPE_ICS = 'text/calendar';

/** Mime-type: Java Archive (JAR) */
const TYPE_JAR = 'application/java-archive';

/** Mime-type: JPEG images */
const TYPE_JPEG = 'image/jpeg';

/** Mime-type: JPEG images */
const TYPE_JPG = 'image/jpeg';

/** Mime-type: JavaScript (ECMAScript) */
const TYPE_JS = 'application/javascript';

/** Mime-type: JSON format */
const TYPE_JSON = 'application/json';

/** Mime-type: Musical Instrument Digital Interface (MIDI) */
const TYPE_MID = 'audio/midi';

/** Mime-type: Musical Instrument Digital Interface (MIDI) */
const TYPE_MIDI = 'audio/midi';

/** Mime-type: MPEG Video */
const TYPE_MPEG = 'video/mpeg';

/** Mime-type: Apple Installer Package */
const TYPE_MPKG = 'application/vnd.apple.installer+xml';

/** Mime-type: OpenDocuemnt presentation document */
const TYPE_ODP = 'application/vnd.oasis.opendocument.presentation';

/** Mime-type: OpenDocuemnt spreadsheet document */
const TYPE_ODS = 'application/vnd.oasis.opendocument.spreadsheet';

/** Mime-type: OpenDocument text document */
const TYPE_ODT = 'application/vnd.oasis.opendocument.text';

/** Mime-type: OGG audio */
const TYPE_OGA = 'audio/ogg';

/** Mime-type: OGG video */
const TYPE_OGV = 'video/ogg';

/** Mime-type: OGG */
const TYPE_OGX = 'application/ogg';

/** Mime-type: Portable Network Graphics */
const TYPE_PNG = 'image/png';

/** Mime-type: Adobe Portable Document Format (PDF) */
const TYPE_PDF = 'application/pdf';

/** Mime-type: Microsoft PowerPoint */
const TYPE_PPT = 'application/vnd.ms-powerpoint';

/** Mime-type: RAR archive */
const TYPE_RAR = 'application/x-rar-compressed';

/** Mime-type: Rich Text Format (RTF) */
const TYPE_RTF = 'application/rtf';

/** Mime-type: Bourne shell script */
const TYPE_SH = 'application/x-sh';

/** Mime-type: Scalable Vector Graphics (SVG) */
const TYPE_SVG = 'image/svg+xml';

/** Mime-type: Small web format (SWF) or Adobe Flash document */
const TYPE_SWF = 'application/x-shockwave-flash';

/** Mime-type: Tape Archive (TAR) */
const TYPE_TAR = 'application/x-tar';

/** Mime-type: Tagged Image File Format (TIFF) */
const TYPE_TIF = 'image/tiff';

/** Mime-type: Tagged Image File Format (TIFF) */
const TYPE_TIFF = 'image/tiff';

/** Mime-type: TrueType Font */
const TYPE_TTF = 'font/ttf';

/** Mime-type: Microsoft Visio */
const TYPE_VSD = 'application/vnd.visio';

/** Mime-type: Waveform Audio Format */
const TYPE_WAV = 'audio/x-wav';

/** Mime-type: WEBM audio */
const TYPE_WEBA = 'audio/webm';

/** Mime-type: WEBM video */
const TYPE_WEBM = 'video/webm';

/** Mime-type: WEBP image */
const TYPE_WEBP = 'image/webp';

/** Mime-type: Web Open Font Format (WOFF) */
const TYPE_WOFF = 'font/woff';

/** Mime-type: Web Open Font Format (WOFF) */
const TYPE_WOFF2 = 'font/woff2';

/** Mime-type: XHTML */
const TYPE_XHTML = 'application/xhtml+xml';

/** Mime-type: Microsoft Excel */
const TYPE_XLS = 'application/vnd.ms-excel';

/** Mime-type: XML */
const TYPE_XML = 'application/xml';

/** Mime-type: XUL */
const TYPE_XUL = 'application/vnd.mozilla.xul+xml';

/** Mime-type: ZIP archive */
const TYPE_ZIP = 'application/zip';

/** Mime-type: 3GPP audio/video container */
const TYPE_3GP = 'video/3gpp';

/** Mime-type: 3GPP audio/video container */
const TYPE_3GP = 'audio/3gpp';

/** Mime-type: 3GPP2 audio/video container */
const TYPE_3G2 = 'video/3gpp2';

/** Mime-type: 3GPP2 audio/video container */
const TYPE_3G2 = 'audio/3gpp2';

/** Mime-type: 7-zip archive */
const TYPE_7Z = 'application/x-7z-compressed';

